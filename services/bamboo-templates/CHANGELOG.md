# @atlaskit/branch-deploy-integrators-plan-template

## 0.0.4

### Patch Changes

- [patch][02f5c59cd0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/02f5c59cd0):

  Add fully qualified docker container urls and make install shell script install latest version of hte CLI

## 0.0.3

### Patch Changes

- [patch][6f8a56abdc](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6f8a56abdc):

  Another update to the plan template

## 0.0.2

### Patch Changes

- [patch][8d1efc5bd2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8d1efc5bd2):

  First firstion of branch deplouy integrator plan
